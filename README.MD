# TASK MANAGER

## DEVELOPER INFO

* **NAME**: Oleg Babeshko

* **E-MAIL**: obabeshko@t1-consulting.ru

* **E-MAIL**: Diez147@gmail.com

## SOFTWARE

* JDK 8

* Intellij Idea

* Windows 10 Pro

## HARDWARE

* **RAM**: 8Gb

* **CPU**: i3 2130

* **SSD**: Samsung EVO 860 Sata 

## BUILD APPLICATION

```shell
mvn clean install
```

## RUN PROGRAM


```shell
java -jar ./task-manager.jar
```
