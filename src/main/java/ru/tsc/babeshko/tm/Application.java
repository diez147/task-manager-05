package ru.tsc.babeshko.tm;

import ru.tsc.babeshko.tm.constant.TerminalConst;

public final class Application {

    public static void main(final String[] args) {
        process(args);
    }

    private static void process(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            case TerminalConst.VERSION:
                showVersion();
                break;
            default:
                showError(arg);
                break;
        }
    }

    public static void showError(String arg) {
        System.err.printf("Error! This argument '%s' not supported...\n", arg);
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - Show program version. \n", TerminalConst.VERSION);
        System.out.printf("%s - Show developer info. \n", TerminalConst.ABOUT);
        System.out.printf("%s - Show list of terminal commands. \n", TerminalConst.HELP);
    }

    private static void showVersion() {
        System.out.println("1.5.0");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Oleg Babeshko");
        System.out.println("Diez147@gmail.com");
    }

}
